import pytest

from app.api import config
from main import app
from app.api.models import Origin, Event
from fastapi.testclient import TestClient

env = {
    "pod_name": "POD_NAME",
    "pod_namespace": "division-1",
    "pod_uid": "123uid",
    "pod_ip": "10.0.0.1",
    "node_name": "c1",
    "node_ip": "143.21.23.21",
    "pdp_port": "8000",
    "pip_port": "8000",
}


@pytest.fixture
def mock_env(monkeypatch):
    for k, v in env.items():
        monkeypatch.setenv(k, v)


def test_info(mock_env):
    client = TestClient(app)
    response = client.get("/info")
    response_obj = Origin.parse_raw(response.text)
    expected_obj = Origin(
        name=env["pod_name"],
        namespace=env["pod_namespace"],
        uid=env["pod_uid"],
        pod_ip=env["pod_ip"],
        node_name=env["node_name"],
        node_ip=env["node_ip"],
    )
    assert response.status_code == 200
    assert response_obj == expected_obj


def test_event(mock_env):
    client = TestClient(app)
    origin = Origin(
        name=env["pod_name"],
        namespace=env["pod_namespace"],
        uid=env["pod_uid"],
        pod_ip=env["pod_ip"],
        node_name=env["node_name"],
        node_ip=env["node_ip"],
    )
    msg = Event(origin=origin, target="target", message="Hello")
    response = client.post("/event", data=str(msg.json()))
    assert response.status_code == 200
